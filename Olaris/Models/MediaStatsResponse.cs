﻿namespace Olaris.Models
{
    public class MediaStatsResponse
    {
        public int MovieCount { get; set; }
        public int SeriesCount { get; set; }
        public int SeasonCount { get; set; }
        public int EpisodeCount { get; set; }
    }
}
