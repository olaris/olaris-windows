﻿namespace Olaris.Models
{
    public class Library
    {
        public int Id { get; set; }
        public int Kind { get; set; } // 0 - movies, 1 - series
        public string Name { get; set; }
        public string FilePath { get; set; }
        public bool IsRefreshing { get; set; }
        public int Backend { get; set; }
        public string RCloneName { get; set; }
        public bool Healthy { get; set; }
        public Movie[] Movies { get; set; }
        public Episode[] Episodes { get; set; }
    }
}
