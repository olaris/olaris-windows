﻿using System.Collections.Generic;

namespace Olaris.Models
{
    public class Season : ICardItem
    {
        public string Name { get; set; }
        public string Overview { get; set; }
        public int SeasonNumber { get; set; }
        public string AirDate { get; set; }
        public string PosterPath { get; set; }
        public int TmdbId { get; set; }
        public List<Episode> Episodes { get; set; }
        public string Uuid { get; set; }
        public int UnwatchedEpisodesCount { get; set; }
        public Series Series { get; set; }

        /// <summary>
        /// Converts the image paths for this Season instance to absolute URLs based on a particular Olaris server.
        /// </summary>
        /// <param name="server"></param>
        public void ConvertToAbsoluteImagePaths(OlarisServer server)
        {
            if (!string.IsNullOrEmpty(PosterPath)) PosterPath = server.GetFullImagePath(PosterPath, PosterSize.w500);
            else if (Series != null && !string.IsNullOrEmpty(Series.PosterPath))
            {
                PosterPath = server.GetFullImagePath(Series.PosterPath, PosterSize.w500);
            }

            if (Series != null) Series.ConvertToAbsoluteImagePaths(server);

            // Convert each episode's still path into an absolute URL, too.
            if (Episodes != null)
            {
                foreach (Episode item in Episodes) item.ConvertToAbsoluteImagePaths(server);
            }
        }

        #region Implementation: ICardItem

        public int CardCount
        {
            get => UnwatchedEpisodesCount;
        }

        public int CardMinCount
        {
            get => 1;
        }
        public string CardPosterPath
        {
            get => PosterPath;
        }
        public string CardTitle
        {
            get => Name;
        }
        public string CardSubtitle
        {
            get => string.Format("{0} Episodes", Episodes.Count);
        }
        public bool CardWatched
        {
            get => true;
        }
        public double CardProgress
        {
            get => 0;
        }

        #endregion
    }
}
