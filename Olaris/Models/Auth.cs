﻿namespace Olaris.Models
{
    public class AuthResponse
    {
        public string Jwt { get; set; }
    }
}
