﻿using System.Collections.Generic;
using System.Linq;

namespace Olaris.Models
{
    public class MovieFile
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int LibraryId { get; set; }
        public string Uuid { get; set; }
        public Stream[] Streams { get; set; }
        public float TotalDuration { get; set; }
        public int FileSize { get; set; }
        public Library Library { get; set; }

        // Returns a list of unique subtitle languages available in this movie file
        public List<string> SubtitleList()
        {
            List<string> subtitles = new List<string>();

            foreach (Stream stream in Streams)
            {
                if (stream.StreamType == "subtitle")
                {
                    subtitles.Add(stream.Language.ToUpper());
                }
            }

            return subtitles.Distinct().ToList();
        }

        // Returns a list of unique audio track languages available in this movie file
        public List<string> AudioTrackList()
        {
            List<string> audiotracks = new List<string>();

            foreach (Stream stream in Streams)
            {
                if (stream.StreamType == "audio")
                {
                    audiotracks.Add(stream.Language.ToUpper());
                }
            }

            return audiotracks.Distinct().ToList();
        }
    }
}
