﻿using LiteDB;
using System;
using System.Threading.Tasks;

namespace Olaris.Models
{
    public class Migration
    {
        public int Id { get; set; }
        public DateTime AppliedAt { get; set; }

        [BsonIgnore]
        public Func<Task> Up { get; set; }
    }
}
