﻿using Microsoft.Toolkit.Collections;
using Olaris.Data;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.UI.Xaml.Data;

namespace Olaris.Models
{
    public class Movie : IMediaItem, ISearchItem, ICardItem
    {
        public string Name { get; set; }
        public string Uuid { get; set; }
        public string Overview { get; set; }
        public int TmdbId { get; set; }
        public PlayState PlayState { get; set; }
        public string Title { get; set; }
        public string Year { get; set; }
        public string ImdbId { get; set; }
        public string BackdropPath { get; set; }
        public string PosterPath { get; set; }
        public List<MovieFile> Files { get; set; }

        /// <summary>
        /// This proxies the Name value so that it can be accessed in a way
        /// that matches the data model in Olaris' back-end.
        /// </summary>
        public string OriginalTitle
        {
            get => Name;
        }

        /// <summary>
        /// Calculates the progress as a double, where 0 = 0%, and 1 = 100%.
        /// </summary>
        public double Progress
        {
            get
            {
                if (Files.Count < 1 || Files.First().TotalDuration == 0) return 0;
                return PlayState.PlayTime / Files.First().TotalDuration;
            }
        }

        /// <summary>
        /// Returns true if the movie has an original title that is different
        /// than it's English title.
        /// </summary>
        public bool HasDifferentOriginalTitle
        {
            get
            {
                return Title != OriginalTitle;
            }
        }

        /// <summary>
        /// Converts the image paths for this Movie instance to absolute URLs based on a particular Olaris server.
        /// </summary>
        /// <param name="server"></param>
        public void ConvertToAbsoluteImagePaths(OlarisServer server)
        {
            if (!string.IsNullOrEmpty(PosterPath))
            {
                PosterPath = server.GetFullImagePath(PosterPath, PosterSize.w500);
            }

            if (!string.IsNullOrEmpty(BackdropPath))
            {
                BackdropPath = server.GetFullImagePath(BackdropPath, BackdropSize.original);
            }
        }

        #region Implementation: ICardItem

        public int CardCount
        {
            get => Files.Count;
        }

        public int CardMinCount
        {
            get => 2;
        }
        public string CardPosterPath
        {
            get => PosterPath;
        }
        public string CardTitle
        {
            get => Title;
        }
        public string CardSubtitle
        {
            get => Year;
        }
        public bool CardWatched
        {
            get => PlayState.Finished;
        }
        public double CardProgress
        {
            get => Progress;
        }

        #endregion
    }

    /// <summary>
    /// Movie collection that can be used as a data source for UWP's infinite scrolling list types.
    /// </summary>
    public class IncrementalLoadingMovieCollection : ObservableCollection<Movie>, ISupportIncrementalLoading
    {
        private int _lastBatchSize = -1;
        private int _lastIndex = 0;
        private IDataSource _dataSource;
        private MovieSortOption _movieSortOption;

        public bool HasMoreItems
        {
            get => _lastBatchSize > 0;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dataSource"></param>
        public IncrementalLoadingMovieCollection(IDataSource dataSource, MovieSortOption movieSortOption)
        {
            _dataSource = dataSource;
            _movieSortOption = movieSortOption;
        }

        // the count is the number requested
        public IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count)
        {
            return AsyncInfo.Run(async cancelToken =>
            {
                var movies = await _dataSource.GetMovies(_lastIndex, 25, _movieSortOption);

                _lastBatchSize = movies.Count;
                _lastIndex = _lastIndex + _lastBatchSize;

                foreach (var item in movies)
                {
                    Add(item);
                }
                
                return new LoadMoreItemsResult { Count = (uint)_lastBatchSize };
            });
        }
    }
}
