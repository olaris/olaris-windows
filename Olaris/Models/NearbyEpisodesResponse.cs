﻿using System.Collections.Generic;

namespace Olaris.Models
{
    public class NearbyEpisodesResponse
    {
        public List<Episode> Previous { get; set; }
        public List<Episode> Next { get; set; }
    }
}
