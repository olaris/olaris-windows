﻿using System.Collections.Generic;

namespace Olaris.Streaming
{
    public class Metadata
    {
        public IList<string> CheckCodecs { get; set; }
    }
}
