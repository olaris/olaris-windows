﻿using Olaris.Models;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class ServerCard : UserControl, INotifyPropertyChanged
    {
        #region Fields

        private App _app;
        private OlarisServer _server;
        private string _serverVersion;

        #endregion

        #region Events

        // The events that can be fired from this control.
        public event EventHandler<OlarisServer> EditClick;
        public event EventHandler<OlarisServer> DeleteClick;
        public event EventHandler<OlarisServer> SetActiveClick;

        #endregion

        #region Constructors

        public ServerCard()
        {
            _app = (App)Application.Current;
            InitializeComponent();
        }

        #endregion

        #region Properties

        public OlarisServer Server
        {
            get => _server;
            set
            {
                _server = value;
                _serverVersion = null;
                NotifyPropertyChanged(nameof(_serverVersion));
                NotifyPropertyChanged(nameof(Server));
                NotifyPropertyChanged(nameof(IsActive));

                GetServerVersion();
            }
        }

        private async void GetServerVersion()
        {
            LoadingSpinner.IsActive = true;

            if (_server == null)
            {
                return;
            }

            try
            {
                var versionString = await _server.GetVersion();
                _serverVersion = versionString;
            }
            catch
            {
                _serverVersion = "Error Fetching Version";
            }

            LoadingSpinner.IsActive = false;
            NotifyPropertyChanged(nameof(_serverVersion));
        }

        /// <summary>
        /// Returns true if the current Server is active.
        /// </summary>
        public bool IsActive
        {
            get
            {
                if (Server == null)
                {
                    return false;
                }
                return _app.State.DataSource.ServerId == Server.OlarisServerId;
            }
        }

        #endregion

        #region EventHandlers

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            EditClick?.Invoke(this, Server);
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            DeleteClick?.Invoke(this, Server);
        }

        private void SetActive_Click(object sender, RoutedEventArgs e)
        {
            SetActiveClick?.Invoke(this, Server);
        }

        #endregion

        #region Methods

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Implementation: INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
