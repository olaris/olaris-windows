﻿using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class LoadingRectangle : UserControl
    {
        public new double Height { get; set; }
        public new double Width { get; set; }

        public LoadingRectangle()
        {
            InitializeComponent();
        }
    }
}