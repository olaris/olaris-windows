﻿using LiteDB;
using Olaris.Models;
using System.Collections.Generic;

namespace Olaris
{
    public partial class LocalCache
    {

        /// <summary>
        /// Returns the default server, or null if there isn't one.
        /// </summary>
        /// <returns></returns>
        public OlarisServer GetDefaultOlarisServer()
        {
            var defaultServerId = _applicationSettings.DefaultServer;
            if (defaultServerId == null)
            {
                return null;
            }

            // TODO: what if the default server no longer exists? BOOM?
            var server = GetOlarisServerById(defaultServerId);
            return server;
        }

        /// <summary>
        /// Set the local cache's default Olaris server. This makes that the server that content will load from when the
        /// app starts.
        /// </summary>
        /// <param name="id"></param>
        public void SetDefaultOlarisServer(ObjectId id)
        {
            _applicationSettings.DefaultServer = id;
            SaveApplicationSettings();
        }

        /// <summary>
        /// Attempts to set a new default Olaris server. Returns true if a new
        /// server was chosen, and false if there were no servers to choose.
        /// </summary>
        /// <returns></returns>
        public bool ChooseNewDefaultServer()
        {
            var servers = GetOlarisServers();
            if (servers.Count > 0)
            {
                SetDefaultOlarisServer(servers[0].OlarisServerId);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Save a new OlarisServer to the LocalCache
        /// </summary>
        /// <param name="server"></param>
        public void SaveOlarisServer(OlarisServer server)
        {
            ServerCollection.Upsert(server);
        }

        /// <summary>
        /// Removes an OlarisServer from the local cache.
        /// </summary>
        /// <param name="id"></param>
        public void RemoveOlarisServer(ObjectId id)
        {
            ServerCollection.Delete(id);
        }

        /// <summary>
        /// Updates an OlarisServer in the local cache.
        /// </summary>
        /// <param name="server"></param>
        public void UpdateOlarisServer(OlarisServer server)
        {
            ServerCollection.Update(server);
        }

        /// <summary>
        /// Returns a single Olaris server from the local cache by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OlarisServer GetOlarisServerById(ObjectId id)
        {
            var collection = ServerCollection;
            var olarisServer = collection.FindById(id);
            return olarisServer;
        }

        /// <summary>
        /// Returns a list of all Olaris servers in the local cache.
        /// </summary>
        /// <returns></returns>
        public List<OlarisServer> GetOlarisServers()
        {
            var collection = ServerCollection;
            var olarisServers = collection.FindAll();
            return new List<OlarisServer>(olarisServers);
        }

    }
}
