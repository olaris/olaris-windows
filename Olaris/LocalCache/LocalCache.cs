﻿using LiteDB;
using Olaris.Models;
using System.IO;
using System.Threading;

namespace Olaris
{
    public partial class LocalCache
    {
        private ILiteDatabase _database;
        private static Mutex _settingsMutex = new Mutex();
        private ApplicationSettings _applicationSettings;

        public LocalCache()
        {
            _database = LoadDatabase();
            EnsureIndexes();
            _applicationSettings = RetrieveOrCreateApplicationSettings();
        }

        private void EnsureIndexes()
        {
            LogCollection.EnsureIndex(m => m.TimeStamp);
        }

        private ILiteCollection<ApplicationSettings> SettingCollection
        {
            get { return _database.GetCollection<ApplicationSettings>("settings"); }
        }

        private ILiteCollection<OlarisServer> ServerCollection
        {
            get { return _database.GetCollection<OlarisServer>("servers"); }
        }
        
        private ILiteCollection<LogMessage> LogCollection
        {
            get { return _database.GetCollection<LogMessage>("logs"); }
        }

        private ILiteCollection<Migration> MigrationCollection
        {
            get { return _database.GetCollection<Migration>("migrations"); }
        }

        // Opens the local LiteDB cache and returns a reference to it
        private ILiteDatabase LoadDatabase()
        {
            var databaseName = "LocalCache";
            var filePath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, databaseName);
            return new LiteDatabase(filePath);
        }
    }
}
