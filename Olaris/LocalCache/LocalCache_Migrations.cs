﻿using Olaris.Models;

namespace Olaris
{
    public partial class LocalCache
    {
        /// <summary>
        /// Inserts a new migration into the database.
        /// </summary>
        public void InsertMigration(Migration migration)
        {
            MigrationCollection.Insert(migration);
        }

        /// <summary>
        /// Get a migration by Id
        /// </summary>
        public Migration GetMigrationById(int id)
        {
            var collection = MigrationCollection;
            var migration = collection.FindById(id);
            return migration;
        }
    }
}
