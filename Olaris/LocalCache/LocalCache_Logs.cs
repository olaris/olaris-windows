﻿using Olaris.Models;
using System.Collections.Generic;
using System.Linq;
using LiteDB;

namespace Olaris
{
    public partial class LocalCache
    {
        /// <summary>
        /// Inserts a new log message into the database.
        /// </summary>
        /// <param name="message"></param>
        public void InsertLogMessage(LogMessage message)
        {
            LogCollection.Insert(message);
        }
        
        /// <summary>
        /// Returns a list of all log messages in the database.
        /// </summary>
        /// <returns></returns>
        public IList<LogMessage> GetLogMessages()
        {
            return LogCollection.Find(Query.All("TimeStamp", Query.Descending)).ToList();
        }

        /// <summary>
        /// Prunes the log collection so that only the most recent 50 are kept.
        /// </summary>
        public void PruneOldLogMessages()
        {
            var oldLogs = LogCollection.Find(Query.All("TimeStamp", Query.Descending), skip: 50);
            _database.BeginTrans();
            foreach (LogMessage log in oldLogs)
            {
                LogCollection.Delete(log.LogMessageId);
            }
            _database.Commit();
        }
    }
}
