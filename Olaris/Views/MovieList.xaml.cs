﻿using Olaris.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// Page that displays the list of movies.
    /// </summary>
    public sealed partial class MovieList : Page, INotifyPropertyChanged
    {
        private App _app;
        private IncrementalLoadingMovieCollection _movies;
        private int _movieCount;
        private IList<MovieSortOption> _sortOptions;
        private MovieSortOption _movieSortOption;

        /// <summary>
        /// Required for INotifyPropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public MovieList()
        {
            _app = ((App)Application.Current);
            NavigationCacheMode = NavigationCacheMode.Enabled;
            _movieSortOption = SortOptions[0];
            ResetMovieCollection();
            InitializeComponent();
        }

        /// <summary>
        /// Creates a new movie collection to "reset" the page.
        /// </summary>
        private async Task ResetMovieCollection()
        {
            _movies = new IncrementalLoadingMovieCollection(_app.State.DataSource, _movieSortOption);
            NotifyPropertyChanged(nameof(_movies));

            // Not awaited because we want to immediately get the movie count.
            // The incremental loading collection will take care of updating
            // the UI when it loads items.
            _movies.LoadMoreItemsAsync(1);

            _movieCount = await _app.State.DataSource.GetMovieCount();
            NotifyPropertyChanged(nameof(_movieCount));
        }

        /// <summary>
        /// Required for INotifyPropertyChanged
        /// </summary>
        /// <param name="propertyName">
        /// Name of the parameter that changed.
        /// </param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Property that returns all available MovieSort and SortDirection
        /// combinations.
        /// </summary>
        private IList<MovieSortOption> SortOptions
        {
            get
            {
                if (_sortOptions == null)
                {
                    var values = new List<MovieSortOption>();
                    foreach (MovieSort ms in Enum.GetValues(typeof(MovieSort)))
                    {
                        foreach (SortDirection sd in Enum.GetValues(typeof(SortDirection)))
                        {
                            values.Add(new MovieSortOption
                            {
                                MovieSort = ms,
                                SortDirection = sd
                            });
                        }
                    }
                    _sortOptions = values;
                }

                return _sortOptions;
            }
        }

        /// <summary>
        /// On navigated to, clear any previous selections.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _app.ClearBackdrop();
            MoviesGrid.SelectedItem = null;

            if (e.NavigationMode == NavigationMode.Back)
            {
                return;
            }

            _movieSortOption = SortOptions[0];
            SortSelector.SelectedIndex = 0;
            ResetMovieCollection();
        }

        /// <summary>
        /// Handle click events on the resultslist items. The item could be either a movie or a series.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoviesGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            var selectedItem = e.ClickedItem;
            Frame.Navigate(typeof(MovieDetails), (selectedItem as Movie).Uuid);
        }

        /// <summary>
        /// When the sort order is changed, reset the movie collection
        /// </summary>
        private void SortSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var c = sender as ComboBox;
            var newSortOption = (MovieSortOption)c.SelectedItem;

            // Do nothing if the sort parameters have not changed. This could
            // happen the first time you open the sort dropdown box.
            if (newSortOption == _movieSortOption)
            {
                return;
            }

            _movieSortOption = newSortOption;
            ResetMovieCollection();
        }
    }
}
