﻿using Olaris.Models;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Logs : Page
    {
        private App _app;
        private ObservableCollection<LogMessage> _logMessages;

        public Logs()
        {
            _app = (App)Application.Current;
            _logMessages = new ObservableCollection<LogMessage>();
            GetLogMessages();
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _app.ClearBackdrop();
        }

        private void GetLogMessages()
        {
            _logMessages.Clear();

            foreach (var item in _app.State.LocalCache.GetLogMessages())
            {
                _logMessages.Add(item);
            }
        }
    }
}
