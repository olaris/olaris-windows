﻿using Olaris.Models;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// View that shows a single season of a tv series.
    /// </summary>
    public sealed partial class SeasonView : Page
    {
        private App _app;
        public Season Season { get; set; }
        public ObservableCollection<Episode> Episodes { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public SeasonView()
        {
            _app = ((App)Application.Current);
            InitializeComponent();
        }

        public string Poster
        {
            get
            {
                if (Season == null || string.IsNullOrEmpty(Season.PosterPath))
                {
                    return (string)Application.Current.Resources["DefaultPosterPath"];
                }
                return Season.PosterPath;
            }
        }

        /// <summary>
        /// When navigated to, get the UUID of the Season and fetch the details from the server.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var uuid = (string)e.Parameter;
            GetSeasonInformation(uuid);
        }

        /// <summary>
        /// Gets the season information from the server.
        /// </summary>
        /// <param name="uuid"></param>
        private async void GetSeasonInformation(string uuid)
        {
            try
            {
                Season = await _app.State.DataSource.GetSeasonById(uuid);
                Episodes = new ObservableCollection<Episode>(Season.Episodes);
            }
            catch (Exception ex)
            {
                Frame.Navigate(typeof(Error), new Error.Options
                {
                    Exception = ex,
                    Message = "Error loading season information.",
                    Page = GetType()
                });
                return;
            }

            _app.RequestBackdropFromUri(Season.Series.BackdropPath);
            Bindings.Update();
        }

        /// <summary>
        /// Navigate the the episode that was clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            Episode selectedItem = (Episode)e.ClickedItem;
            Frame.Navigate(typeof(EpisodeDetails), selectedItem.Uuid);
        }

        /// <summary>
        /// Navigate to this season's series.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void SeriesLink_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SeriesView), Season.Series.Uuid);

        }
    }
}
