﻿using Olaris.Models;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// View that shows a tv series.
    /// </summary>
    public sealed partial class SeriesView : Page
    {
        private App _app;
        public Series Series { get; set; }
        public ObservableCollection<Season> Seasons { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public SeriesView()
        {
            _app = ((App)Application.Current);
            InitializeComponent();
        }

        public string Poster
        {
            get
            {
                if (Series == null || string.IsNullOrEmpty(Series.PosterPath))
                {
                    return (string)Application.Current.Resources["DefaultPosterPath"];
                }
                return Series.PosterPath;
            }
        }

        /// <summary>
        /// Attempts to extract the year from the FirstAirDate string.
        /// </summary>
        public String StartYear
        {
            get
            {
                if (Series == null) return "";
                else if (Series.FirstAirDate.Length >= 4) return Series.FirstAirDate.Substring(0, 4);
                else return "Year Unknown";
            }
        }

        /// <summary>
        /// When navigated to, get the UUID of the Series and fetch the details from the server.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var uuid = (string)e.Parameter;
            GetSeriesInformation(uuid);
        }

        /// <summary>
        /// Gets the series information from the server.
        /// </summary>
        /// <param name="uuid"></param>
        private async void GetSeriesInformation(string uuid)
        {
            try
            {
                Series = await _app.State.DataSource.GetSeriesById(uuid);
                Seasons = new ObservableCollection<Season>(Series.Seasons);
            }
            catch (Exception ex)
            {
                Frame.Navigate(typeof(Error), new Error.Options
                {
                    Exception = ex,
                    Message = "Error loading series information.",
                    Page = GetType()
                });
                return;
            }

            _app.RequestBackdropFromUri(Series.BackdropPath);
            Bindings.Update();
        }

        /// <summary>
        /// Navigate the the season that was clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            Season selectedItem = (Season)e.ClickedItem;
            Frame.Navigate(typeof(SeasonView), selectedItem.Uuid);
        }
    }
}
