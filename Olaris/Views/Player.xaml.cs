﻿using Olaris.Models;
using Olaris.Streaming;
using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Media.Streaming.Adaptive;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// A page that displays the media player for any type of media item.
    /// </summary>
    public sealed partial class Player : Page
    {
        #region Fields

        private App _app;
        private Session _streamingSession;
        private MediaPlayer _mediaPlayer;
        private MediaSource _mediaSource;
        private Options _options;
        private DispatcherTimer _timer;
        private DateTime _lastPointerMovement;
        private DateTime _lastProgressUpdate;

        #endregion

        #region Constructors

        public Player()
        {
            _app = (App)Application.Current;
            InitializeComponent();
        }

        #endregion

        #region SubClasses

        /// <summary>
        /// The options that get passed in when you launch the player view.
        /// </summary>
        public class Options
        {
            public string MediaFileUuid { get; set; }
            public string MediaItemUuid { get; set; }
            public TimeSpan StartAt { get; set; }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns the percent of the total duration that has been played.
        /// </summary>
        private double PercentComplete
        {
            get
            {
                var duration = _mediaSource.Duration ?? TimeSpan.FromSeconds(0);
                var position = _mediaPlayer.PlaybackSession.Position.TotalSeconds;

                // Prevent division by zero - might make sense for the to return a nullable double, to represent this
                // case with a null instead of a 0.
                if (duration.TotalSeconds == 0) return 0;

                return position / duration.TotalSeconds;
            }
        }

        /// <summary>
        /// Indicates if playback has finished or not.
        /// </summary>
        private bool IsFinished
        {
            get => PercentComplete > 0.95;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Runs when this page is navigated to. Obviously.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Window.Current.SetTitleBar(AppTitleBar);

            _options = e.Parameter as Options;
            SetupPlayer();
        }

        /// <summary>
        /// Gets a streaming ticket and sets up the player
        /// </summary>
        private async void SetupPlayer()
        {
            try
            {
                await GetStreamingSession();
                await CreateMediaSource();
                CreateMediaPlayer();
                StartTimer();
            }
            catch (Exception _)
            {
                // TODO: Figure out a good way to show the error here
                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.GoBack();
            }
        }

        /// <summary>
        /// Create a media source from the DASH URL returned from Olaris. The media source can then be set as the source
        /// for a MediaPlayer.
        /// </summary>
        private async Task CreateMediaSource()
        {
            var url = await _streamingSession.GetDashUrl();
            var mediaSourceCreationResult = await AdaptiveMediaSource.CreateFromUriAsync(url.ToUri());
            if (mediaSourceCreationResult.Status != AdaptiveMediaSourceCreationStatus.Success)
            {
                throw new ApplicationException("could not create adaptive media source");
            }

            var ams = mediaSourceCreationResult.MediaSource;
            ams.InitialBitrate = ams.AvailableBitrates.Max();
            _mediaSource = MediaSource.CreateFromAdaptiveMediaSource(ams);
        }

        /// <summary>
        /// Use the MediaSource that has already been created (right??), and wrap it in a MediaPlayer.
        /// </summary>
        private void CreateMediaPlayer()
        {
            _mediaPlayer = new MediaPlayer();
            _mediaPlayer.Volume = _app.State.LocalCache.ApplicationSettings.Volume;
            _mediaPlayer.VolumeChanged += _onVolumeChanged;
            _mediaPlayer.MediaEnded += _mediaPlayer_MediaEnded;
            _mediaPlayer.Source = _mediaSource;
            _mediaPlayerElement.SetMediaPlayer(_mediaPlayer);
            _mediaPlayerElement.PointerMoved += _mediaPlayerElement_PointerMoved;
            _mediaPlayer.PlaybackSession.Position = _options.StartAt;
            _mediaPlayer.Play();
        }

        private void _mediaPlayerElement_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if (Window.Current.CoreWindow.PointerCursor == null)
            {
                Window.Current.CoreWindow.PointerCursor = new CoreCursor(CoreCursorType.Arrow, 0);
            }

            _lastPointerMovement = DateTime.Now;
        }

        /// <summary>
        /// Exit the player when the media ends.
        /// </summary>
        private async void _mediaPlayer_MediaEnded(MediaPlayer sender, object args)
        {
            await _exitPlayer();
        }

        private void _onVolumeChanged(MediaPlayer sender, object e)
        {
            _app.State.LocalCache.SaveVolumePreference(sender.Volume);
        }

        /// <summary>
        /// Fetch a streaming ticket for this media item from the data source.
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        private async Task GetStreamingSession()
        {
            try
            {
                _streamingSession = await _app.State.DataSource.CreateStreamingSession(_options.MediaFileUuid);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("could not create streaming ticket", ex);
            }
        }

        /// <summary>
        /// The timer updates the server with the current progress.
        /// </summary>
        private void StartTimer()
        {
            _timer = new DispatcherTimer();
            _timer.Tick += timer_Tick;
            _timer.Interval = new TimeSpan(0, 0, 1);
            _timer.Start();
        }

        /// <summary>
        /// Executes on every tick.
        /// </summary>
        private async void timer_Tick(object sender, object e)
        {
            // Hide the cursor if it hasn't moved in 2 seconds.
            if (_mediaPlayer.PlaybackSession.PlaybackState == MediaPlaybackState.Playing && (DateTime.Now - _lastPointerMovement).TotalSeconds > 2)
            {
                Window.Current.CoreWindow.PointerCursor = null;
            }

            // Update the play state every 5 seconds if the session is playing.
            if (_mediaPlayer.PlaybackSession.PlaybackState == MediaPlaybackState.Playing && (DateTime.Now - _lastProgressUpdate).TotalSeconds > 5)
            {
                await UpdatePlayState();
            }
        }

        /// <summary>
        /// Update the play state on the server. Send a 0 position if the play state is finished. I think the web player
        /// does this. Not sure if it's necessary.
        /// </summary>
        private async Task UpdatePlayState()
        {
            double position = IsFinished ? 0 : _mediaPlayer.PlaybackSession.Position.TotalSeconds;
            try
            {
                await _app.State.DataSource.CreatePlayState(_options.MediaItemUuid, IsFinished, (float)position);
                _lastProgressUpdate = DateTime.Now;
            }
            catch (Exception _)
            {
                // TODO: figure out what to do with this exception. Probably shouldn't kill the playback session just
                // because an update ping failed.
            }
        }

        /// <summary>
        /// Closes the player and goes back to the previous page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void _playerControls_Exited(object sender, EventArgs e)
        {
            await _exitPlayer();
        }

        /// <summary>
        /// Cleans up and exits the player, returning to the previous page.
        /// </summary>
        private async Task _exitPlayer()
        {
            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                async () =>
                {
                    _timer.Stop();
                    _mediaPlayer.Pause();
                    await UpdatePlayState();
                    _mediaPlayer.Source = null;
                    _mediaSource.Dispose();

                    if (Window.Current.CoreWindow.PointerCursor == null)
                    {
                        Window.Current.CoreWindow.PointerCursor = new CoreCursor(CoreCursorType.Arrow, 0);
                    }

                    Frame rootFrame = Window.Current.Content as Frame;
                    rootFrame.GoBack();
                }
            );
        }

        /// <summary>
        /// Moves the playback position backwards by 10 seconds
        /// </summary>
        private void _playerControls_SkipBackwards(object sender, EventArgs e)
        {
            _mediaPlayer.PlaybackSession.Position = _mediaPlayer.PlaybackSession.Position.Subtract(new TimeSpan(0, 0, 10));
        }

        /// <summary>
        /// Moves the playback position forward by 30 seconds
        /// </summary>
        private void _playerControls_SkipForwards(object sender, EventArgs e)
        {
            _mediaPlayer.PlaybackSession.Position = _mediaPlayer.PlaybackSession.Position.Add(new TimeSpan(0, 0, 30));
        }

        #endregion
    }
}
