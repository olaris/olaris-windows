﻿using Olaris.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// Page that displays the search results.
    /// </summary>
    public sealed partial class SearchResults : Page, INotifyPropertyChanged
    {
        #region Fields

        private App _app;
        private string _query;

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors

        public SearchResults()
        {
            _app = ((App)Application.Current);
            NavigationCacheMode = NavigationCacheMode.Enabled;
            InitializeComponent();
        }

        #endregion

        #region Properties

        public ObservableCollection<ISearchItem> Results { get; set; }

        #endregion

        #region Methods

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets the search results from the server
        /// </summary>
        /// <param name="query"></param>
        private async void GetSearchResults(string query)
        {
            // Since we are caching the page, clear old results if a new query is run.
            if (Results != null)
            {
                Results.Clear();
                HideMessage();
            }
            else
            {
                Results = new ObservableCollection<ISearchItem>();
            }

            if (string.IsNullOrEmpty(query))
            {
                ShowMessage("Enter a search query to see results");
                return;
            }

            try
            {
                LoadingIndicator.IsActive = true;
                var results = await _app.State.DataSource.GetSearchResults(query);
                LoadingIndicator.IsActive = false;
                results.ForEach(result => Results.Add(result));
            }
            catch (Exception ex)
            {
                Frame.Navigate(typeof(Error), new Error.Options
                {
                    Exception = ex,
                    Message = "Error loading search results.",
                    Page = GetType()
                });
                return;
            }

            NotifyPropertyChanged("Results");

            if (Results.Count == 0)
            {
                ShowMessage(string.Format("No results found for query \"{0}\"", query));
            }
        }

        private void ShowMessage(string message)
        {
            NoResultsText.Text = message;
            NoResultsMessage.Visibility = Visibility.Visible;
        }

        private void HideMessage()
        {
            NoResultsMessage.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handle click events on the resultslist items. The item could be either a movie or a series.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Result_ItemClick(object sender, ItemClickEventArgs e)
        {
            var selectedItem = e.ClickedItem;
            if (selectedItem is Movie)
            {
                Frame.Navigate(typeof(MovieDetails), (selectedItem as Movie).Uuid);
            }
            if (selectedItem is Series)
            {
                Frame.Navigate(typeof(SeriesView), (selectedItem as Series).Uuid);
            }
        }

        /// <summary>
        /// When navigated to, get the results of the search query from the server.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _app.ClearBackdrop();

            ResultsGrid.SelectedItem = null;

            if (_query == null || (string)e.Parameter != _query)
            {
                _query = (string)e.Parameter;
                GetSearchResults(_query);
            }
        }

        #endregion
    }
}
