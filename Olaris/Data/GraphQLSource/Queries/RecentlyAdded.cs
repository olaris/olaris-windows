﻿using GraphQL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Olaris.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {
        
        public class RecentlyAddedResponse
        {
            [JsonProperty("recentlyAdded")]
            public List<JObject> RecentlyAdded { get; set; }
        }

        public async Task<List<IMediaItem>> GetRecentlyAddedMediaItems()
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                {
                    recentlyAdded {
                        ... on Movie {
                            type: __typename
                            uuid
                            title
                            year
                            posterPath

                            playState {
                                finished
                                playtime
                            }

                            files {
                                totalDuration
                            }
                        }
                        ... on Episode {
                            type: __typename
                            uuid
                            name
                            episodeNumber

                            season {
                                seasonNumber
                                posterPath
                                uuid

                                series {
                                    name
                                    uuid
                                    posterPath
                                }
                            }

                            playState {
                                finished
                                playtime
                            }

                            files {
                                totalDuration
                            }
                        }
                    }
                }"
            };

            var response = await client.SendQueryAsync<RecentlyAddedResponse>(request);

            var recentlyAdded = new List<IMediaItem>();

            foreach (JObject item in response.Data.RecentlyAdded)
            {
                if (item.Value<string>("type") == "Movie")
                {
                    var thisMovie = item.ToObject<Movie>();
                    thisMovie.ConvertToAbsoluteImagePaths(_olarisServer);
                    recentlyAdded.Add(thisMovie);
                }
                if (item.Value<string>("type") == "Episode")
                {
                    var thisEpisode = item.ToObject<Episode>();
                    thisEpisode.ConvertToAbsoluteImagePaths(_olarisServer);
                    recentlyAdded.Add(thisEpisode);
                }
            }

            return recentlyAdded;
        }

    }
}
