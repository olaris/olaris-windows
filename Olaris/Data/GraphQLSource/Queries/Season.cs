﻿using GraphQL;
using Newtonsoft.Json;
using Olaris.Models;
using System;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {
        /// <summary>
        /// Data structure of the season graphql query response.
        /// </summary>
        public class SeasonResponse
        {
            [JsonProperty("season")]
            public Season Season { get; set; }
        }

        /// <summary>
        /// Gets a season from the server by its UUID.
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public async Task<Season> GetSeasonById(string uuid)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query season($uuid: String!) {
                    season(uuid: $uuid) {
                        type: __typename
                        name
                        overview
                        seasonNumber
                        airDate
                        posterPath
                        uuid
                        unwatchedEpisodesCount

                        series {
                            posterPath
                            backdropPath
                            name
                            overview
                            uuid
                            seasons {
                                name
                                uuid
                                seasonNumber
                            }
                        }

                        episodes {
                            type: __typename
                            name
                            overview
                            uuid
                            stillPath
                            episodeNumber
                            id: tmdbID

                            playState {
                                finished
                                playtime
                            }

                            files {
                                totalDuration
                            }
                        }
                    }
                }",
                OperationName = "season",
                Variables = new
                {
                    uuid = uuid
                }
            };

            var response = await client.SendQueryAsync<SeasonResponse>(request);

            response.Data.Season.Episodes.Sort((x,y) => x.EpisodeNumber - y.EpisodeNumber);
            response.Data.Season.ConvertToAbsoluteImagePaths(_olarisServer);

            return response.Data.Season;
        }

    }
}
