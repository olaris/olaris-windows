﻿using GraphQL;
using Newtonsoft.Json;
using Olaris.Models;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {
        public class CreatePlayStateResponse
        {
            [JsonProperty("createPlayState")]
            public PlayStateResponse CreatePlayState { get; set; }
        }

        /// <summary>
        /// Updates the server-side play state of the media item identified by the uuid.
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="finished"></param>
        /// <param name="playTime"></param>
        /// <returns></returns>
        public async Task<PlayStateResponse> CreatePlayState(string uuid, bool finished, float playTime)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                mutation createPlayState($uuid: String!, $finished: Boolean!, $playtime: Float!) {
                    createPlayState(uuid: $uuid, finished: $finished, playtime: $playtime) {
                        uuid
                        playState {
                            finished
                            playtime
                        }
                    }
                }",

                OperationName = "createPlayState",
                Variables = new
                {
                    uuid = uuid,
                    finished = finished,
                    playtime = playTime
                }
            };

            var response = await client.SendMutationAsync<CreatePlayStateResponse>(request);
            return response.Data.CreatePlayState;
        }

    }
}
