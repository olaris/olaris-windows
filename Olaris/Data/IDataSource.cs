﻿using LiteDB;
using Olaris.Models;
using Olaris.Streaming;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public interface IDataSource
    {
        #region Queries
        Task<Movie> GetMovieById(string uuid);
        Task<List<Movie>> GetMovies(int offset, int limit, MovieSortOption movieSortOption);
        Task<List<IMediaItem>> GetUpNextMediaItems();
        Task<List<IMediaItem>> GetRecentlyAddedMediaItems();
        Task<Episode> GetEpisodeById(string uuid);
        Task<Season> GetSeasonById(string uuid);
        Task<Series> GetSeriesById(string uuid);
        Task<List<Series>> GetSeries(int offset, int limit, SeriesSortOption seriesSortOption);
        Task<List<ISearchItem>> GetSearchResults(string query);
        Task<int> GetMovieCount();
        Task<int> GetSeriesCount();
        Task<NearbyEpisodesResponse> GetNearbyEpisodes(string episodeUuid, int count);
        #endregion

        #region Mutations
        Task<CreateSTResponse> CreateStreamingTicket(string uuid);
        Task<PlayStateResponse> CreatePlayState(string uuid, bool finished, float playTime);
        #endregion

        #region Misc
        Task<Session> CreateStreamingSession(string uuid);
        ObjectId ServerId { get; }
        #endregion
    }
}
