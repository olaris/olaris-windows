﻿using Olaris.Data;

namespace Olaris
{
    public class ApplicationState
    {

        public IDataSource DataSource { get; set; }
        public LocalCache LocalCache { get; set; }

        public ApplicationState()
        {
            LocalCache = new LocalCache();
            var defaultServer = LocalCache.GetDefaultOlarisServer();
            if (defaultServer != null)
            {
                var dataSource = new GraphQLSource(defaultServer);
                DataSource = dataSource;
            }

        }

    }
}
